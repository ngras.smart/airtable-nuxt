import { defineNuxtConfig } from 'nuxt/config';
import airtable from '..';

export default defineNuxtConfig({
  modules: [airtable],
  runtimeConfig: {
    airtable: {
      apiKey: process.env.AIRTABLE_API_KEY,
      base: process.env.AIRTABLE_BASE
    }
  }
});
