import { H3Event } from 'h3';
export default defineEventHandler(async (event: H3Event) => {
  console.log('Event in app:', event.context);
  const { table } = await event.context._serverAirtableClient;
  console.log('From app server api:', table);

  return { table };
});
