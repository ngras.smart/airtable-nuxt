import { resolve } from 'path';
import { fileURLToPath } from 'url';
import { defu } from 'defu';
import {
  defineNuxtModule,
  addPlugin,
  createResolver,
  resolveModule,
  extendViteConfig,
  addServerHandler
} from '@nuxt/kit';
import type { NuxtModule } from '@nuxt/schema';

export interface ModuleOptions {
  /**
   * Airtable API Key
   * @default process.env.AIRTABLE_API_KEY
   * @example 'key1234567890'
   * @type string
   */
  apiKey: string;

  /**
   * Airtable base selector
   * @default process.env.AIRTABLE_BASE
   * @example 'appa1B2c3'
   * @type string
   */
  base: string;
  /**
   * Airtable URL endpoint
   * @default process.env.AIRTABLE_ENDPOINT_URL
   * @example 'https://api.airtable.com'
   * @type string
   */
  endpointUrl?: string;

  /**
   * Timeout requests in seconds
   * @default process.env.AIRTABLE_REQUEST_TIMEOUT
   * @example 300000
   * @type number
   */
  requestTimeout?: number;
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'nuxt-airtable',
    configKey: 'airtable',
    compatibility: {
      nuxt: '^3.0.0-rc.12'
    }
  },
  defaults: {
    apiKey: process.env.AIRTABLE_API_KEY as string,
    base: process.env.AIRTABLE_BASE as string,
    endpointUrl:
      (process.env.AIRTABLE_ENDPOINT_URL as string) ||
      'https://api.airtable.com',
    requestTimeout:
      (Number(process.env.AIRTABLE_REQUEST_TIMEOUT) as number) || 300000
  },
  setup(options, nuxt) {
    const { resolve } = createResolver(import.meta.url);

    if (!options.apiKey) {
      console.warn('missing `AIRTABLE_API_KEY` in .env');
    }

    if (!options.base) {
      console.warn('missing `AIRTABLE_BASE` in .env');
    }

    // Public runtime Config
    nuxt.options.runtimeConfig.public.airtable = defu(
      nuxt.options.runtimeConfig.public.airtable,
      {
        apiKey: options.apiKey,
        base: options.base,
        endpointUrl: options.endpointUrl,
        requestTimeout: options.requestTimeout
      }
    );

    nuxt.options.runtimeConfig.airtable = defu(
      nuxt.options.runtimeConfig.airtable,
      {
        apiKey: options.apiKey,
        base: options.base,
        endpointUrl: options.endpointUrl,
        requestTimeout: options.requestTimeout
      }
    );

    // Transpile runtime
    const runtimeDir = fileURLToPath(new URL('./runtime', import.meta.url));
    nuxt.options.build.transpile.push(runtimeDir);

    // Add server plugin to load
    addPlugin(resolve(runtimeDir, 'airtable'));

    // Add airtable composables
    nuxt.hook('imports:dirs', (dirs) => {
      dirs.push(resolve(runtimeDir, 'composables'));
    });

    const handler = resolve(runtimeDir, 'server', 'serverAirtableClient');
    const serverHandler = {
      middleware: true,
      handler
    };
    addServerHandler(serverHandler);

    // Optimize
    extendViteConfig((config) => {
      config.optimizeDeps = config.optimizeDeps || {};
      config.optimizeDeps.include = config.optimizeDeps.include || [];
    });
  }
});
