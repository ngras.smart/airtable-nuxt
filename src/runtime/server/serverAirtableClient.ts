import { AirtableBase } from 'airtable/lib/airtable_base';
import { H3Event, defineEventHandler } from 'h3';
import { useAirtableConfig } from './../composables/useAirtableConfig';

export default defineEventHandler(async (event: H3Event) => {
  const airtable = useAirtableConfig();
  event.context._serverAirtableClient = airtable;
  console.log('Server event:', airtable.airtable);
  await event.context._serverAirtableClient;
});
