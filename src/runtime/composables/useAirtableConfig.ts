import Airtable, { AirtableOptions } from 'airtable';
import type { AirtableBase } from 'airtable/lib/airtable_base';
import { useRuntimeConfig } from '#app';

export const useAirtableConfig = () => {
  const airtable: AirtableBase = new Airtable({
    apiKey: useRuntimeConfig().airtable.apiKey,
    endpointUrl: useRuntimeConfig().airtable.endpointUrl,
    requestTimeout: useRuntimeConfig().airtable.requestTimeout
  }).base(useRuntimeConfig().airtable.base);

  return { airtable };
};
