import { useAirtableConfig } from './useAirtableConfig';

export const useAirtableClient = (table: string) => {
  const config = useAirtableConfig();

  const nuxtApp = useNuxtApp();

  console.log('Nuxt app:', nuxtApp);
};
